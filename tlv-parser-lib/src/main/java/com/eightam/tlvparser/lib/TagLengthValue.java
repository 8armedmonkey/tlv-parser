package com.eightam.tlvparser.lib;

public class TagLengthValue {

    private Tag tag;
    private Length length;
    private Value value;

    public TagLengthValue(Tag tag, Length length, Value value) {
        this.tag = tag;
        this.length = length;
        this.value = value;
    }

    public Tag getTag() {
        return tag;
    }

    public Length getLength() {
        return length;
    }

    public Value getValue() {
        return value;
    }

}
