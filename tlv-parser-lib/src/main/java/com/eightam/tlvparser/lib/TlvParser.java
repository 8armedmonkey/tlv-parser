package com.eightam.tlvparser.lib;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TlvParser {

    public static List<TagLengthValue> decodeAsList(byte[] tlv) {
        ByteBuffer bb = ByteBuffer.wrap(tlv);
        List<TagLengthValue> list = new ArrayList<>();

        readTlv(bb, list);

        return list;
    }

    public static Map<Tag, Value> decodeAsMap(byte[] tlv) {
        Map<Tag, Value> map = new HashMap<>();
        List<TagLengthValue> list = decodeAsList(tlv);

        for (TagLengthValue item : list) {
            map.put(item.getTag(), item.getValue());
        }
        return map;
    }

    public static Value find(byte[] tlv, Tag tag) {
        return decodeAsMap(tlv).get(tag);
    }

    static void readTlv(ByteBuffer bb, List<TagLengthValue> list) {
        while (bb.hasRemaining()) {
            Tag tag = readTag(bb);
            Length length = readLength(bb);
            Value value = readValue(bb, length.getLength());

            list.add(new TagLengthValue(tag, length, value));
        }
    }

    static Tag readTag(ByteBuffer bb) {
        ByteArrayOutputStream o = new ByteArrayOutputStream();

        byte b = bb.get();
        o.write(b);

        if (Tag.shouldReadSubsequentBytes(b)) {
            do {
                b = bb.get();
                o.write(b);
            } while (Tag.isNotLastTagByteOfMultiBytesTag(b));
        }
        return new Tag(o.toByteArray());
    }

    static Length readLength(ByteBuffer bb) {
        ByteArrayOutputStream o = new ByteArrayOutputStream();

        byte b = bb.get();
        o.write(b);

        if (Length.isMoreThanOneByteLong(b)) {
            int numOfSubsequentLengthBytes = Length.getNumOfSubsequentLengthBytes(b);

            for (int i = 0; i < numOfSubsequentLengthBytes; i++) {
                b = bb.get();
                o.write(b);
            }
        }
        return new Length(o.toByteArray());
    }

    static Value readValue(ByteBuffer bb, int length) {
        int readLength = Math.min(bb.remaining(), length);
        byte[] bytes = new byte[readLength];

        bb.get(bytes);

        return new Value(bytes);
    }

}
