package com.eightam.tlvparser.lib;

import java.util.Arrays;

public class Tag {

    private byte[] bytes;

    public Tag(byte... bytes) {
        this.bytes = bytes;
    }

    public static Tag fromHex(String hex) {
        int len = hex.length();
        byte[] bytes = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            bytes[i / 2] = (byte) ((Character.digit(hex.charAt(i), 16) << 4)
                    + Character.digit(hex.charAt(i + 1), 16));
        }
        return new Tag(bytes);
    }

    public static boolean shouldReadSubsequentBytes(byte b) {
        return (b & 0x1F) == 0x1F;
    }

    public static boolean isNotLastTagByteOfMultiBytesTag(byte b) {
        return (b & 0x80) == 0x80;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(bytes);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Tag) {
            return Arrays.equals(bytes, ((Tag) obj).bytes);
        }
        return super.equals(obj);
    }

    public byte[] getTag() {
        return bytes;
    }

}
