package com.eightam.tlvparser.lib;

import java.util.Arrays;

public class Length {

    private byte[] bytes;
    private int length;

    public Length(byte... bytes) {
        this.bytes = bytes;
        calculateLength();
    }

    public static boolean isMoreThanOneByteLong(byte b) {
        return (b & 0x80) == 0x80;
    }

    public static int getNumOfSubsequentLengthBytes(byte b) {
        return (b & 0x7F);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(bytes);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Length) {
            return Arrays.equals(bytes, ((Length) obj).bytes);
        }
        return super.equals(obj);
    }

    public int getLength() {
        return length;
    }

    public byte[] getBytes() {
        return bytes;
    }

    private void calculateLength() {
        length = 0;

        if (bytes.length > 1) {
            for (int i = 1; i < bytes.length; i++) {
                length += (bytes[i] & 0xFF);

                if (i < bytes.length) {
                    length *= 256;
                }
            }
        } else {
            length = bytes[0];
        }
    }

}
