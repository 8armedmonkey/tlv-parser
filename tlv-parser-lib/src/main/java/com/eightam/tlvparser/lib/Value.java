package com.eightam.tlvparser.lib;

import java.util.Arrays;

public class Value {

    private byte[] bytes;

    public Value(byte... bytes) {
        this.bytes = bytes;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(bytes);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Value) {
            return Arrays.equals(bytes, ((Value) obj).bytes);
        }
        return super.equals(obj);
    }

    public byte[] getValue() {
        return bytes;
    }

}
