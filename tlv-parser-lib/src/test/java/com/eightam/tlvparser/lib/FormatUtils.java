package com.eightam.tlvparser.lib;

import java.math.BigInteger;
import java.nio.CharBuffer;
import java.util.Arrays;

public class FormatUtils {

    protected static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();

    /**
     * Source http://stackoverflow.com/a/8544521
     *
     * @param octetStringSize The length of the octet string (byte array).
     * @param i               The integer.
     * @return Octet string.
     */
    public static byte[] toOctetString(int octetStringSize, BigInteger i) {
        if (i.signum() < 0) {
            throw new IllegalArgumentException("BigInteger argument should not be negative.");
        }

        if (octetStringSize <= 0) {
            throw new IllegalArgumentException("Octet string size should be greater than 0.");
        }

        if (i.bitLength() > octetStringSize * Byte.SIZE) {
            throw new IllegalArgumentException("Octet string size is not enough to store the BigInteger argument.");
        }

        byte[] signedEncoding = i.toByteArray();
        int signedEncodingLength = signedEncoding.length;

        if (signedEncodingLength == octetStringSize) {
            return signedEncoding;
        }

        byte[] unsignedEncoding = new byte[octetStringSize];
        if (signedEncoding[0] == (byte) 0x00) {
            // skip first padding byte to create a (positive) unsigned encoding for this number
            System.arraycopy(signedEncoding, 1, unsignedEncoding,
                    octetStringSize - signedEncodingLength + 1,
                    signedEncodingLength - 1);

        } else {
            System.arraycopy(signedEncoding, 0, unsignedEncoding,
                    octetStringSize - signedEncodingLength,
                    signedEncodingLength);
        }
        return unsignedEncoding;
    }

    public static void toHexString(CharBuffer buffer, byte b) {
        int v = b & 0xFF;
        buffer.append(HEX_ARRAY[v >>> 4]);
        buffer.append(HEX_ARRAY[v & 0x0F]);
    }

    public static String toHexString(byte[] bytes) {
        CharBuffer buffer = toHexChars(bytes);
        return buffer.toString();
    }

    public static byte[] fromHexString(CharSequence s) {
        if (s == null) {
            return null;
        }
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    public static CharBuffer toHexChars(byte[] bytes) {
        CharBuffer buffer = CharBuffer.allocate(bytes.length * 2);
        for (byte aByte : bytes) {
            toHexString(buffer, aByte);
        }
        buffer.position(0);
        return buffer;
    }

    public static byte[] fromDecimalChars(char[] chars) {
        byte[] bytes = new byte[chars.length / 2];

        for (int i = 0; i < chars.length; i += 2) {
            bytes[i / 2] = (byte) ((Character.digit(chars[i], 10) << 4)
                    + Character.digit(chars[i + 1], 10));
        }
        return bytes;
    }

    public static String padLeft(String input, char c, int length) {
        char[] inputAsCharArray = input.toCharArray();
        int padLength = length - inputAsCharArray.length;

        if (padLength > 0) {
            char[] output = new char[length];

            Arrays.fill(output, 0, padLength, c);
            System.arraycopy(inputAsCharArray, 0, output, padLength, inputAsCharArray.length);

            return new String(output);

        } else {
            return new String(inputAsCharArray);

        }
    }

}
