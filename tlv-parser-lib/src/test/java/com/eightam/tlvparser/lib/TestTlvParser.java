package com.eightam.tlvparser.lib;

import io.github.binaryfoo.DecodedData;
import io.github.binaryfoo.EmvTags;
import io.github.binaryfoo.decoders.DecodeSession;
import io.github.binaryfoo.decoders.TLVDecoder;
import io.github.binaryfoo.tlv.BerTlv;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;

public class TestTlvParser {

    private static final String TEST_COMPLEX_HEX_TLV = "" +
            "09060000000000050A02097889063030313032338A033030" +
            "309F1C0841424E30303420209F4E32544553542041424E20" +
            "202020202020202020202020203B3B416D7374657264616D" +
            "202020203B3B3B3B4E4C443B3B3B3B3B3B9F020600000000" +
            "00055F2A0209789F1A020528950500200080009A03160519" +
            "9C01009F3501219F10120014A0400022000000000055A257" +
            "4BC800FF9F2608E30BBA8C1CFA94109F36020018DFFF4524" +
            "000000000000000000000000000000000000000000000000" +
            "000000000000000000000000";

    private static final String TEST_SIMPLE_HEX_TLV_89 =
            "8906303031303233";

    private static final String TEST_SIMPLE_HEX_TLV_8A =
            "8A03303030";

    private static final String TEST_SIMPLE_HEX_TLV_9F02 =
            "9F0206000000000005";

    private static final String TEST_SIMPLE_HEX_TLV_5F2A =
            "5F2A020978";

    private static final String TEST_SIMPLE_HEX_TLV_9F1C =
            "9F1C0841424E3030342020";

    private static final String TEST_SIMPLE_HEX_TLV_DFFF45 =
            "DFFF4524000000000000000000000000000000000000000000000000000000000000000000000000";

    private static final int TEST_SIMPLE_HEX_TLV_89_LENGTH = 6;

    private static final int TEST_SIMPLE_HEX_TLV_8A_LENGTH = 3;

    private static final int TEST_SIMPLE_HEX_TLV_9F02_LENGTH = 6;

    private static final int TEST_SIMPLE_HEX_TLV_5F2A_LENGTH = 2;

    private static final int TEST_SIMPLE_HEX_TLV_9F1C_LENGTH = 8;

    private static final int TEST_SIMPLE_HEX_TLV_DFFF45_LENGTH = 36;

    private static final Tag TAG_89 = new Tag((byte) 0x89);

    private static final Tag TAG_8A = new Tag((byte) 0x8A);

    private static final Tag TAG_9F02 = new Tag((byte) 0x9F, (byte) 0x02);

    private static final Tag TAG_5F2A = new Tag((byte) 0x5F, (byte) 0x2A);

    private static final Tag TAG_9F1C = new Tag((byte) 0x9F, (byte) 0x1C);

    private static final Tag TAG_DFFF45 = new Tag((byte) 0xDF, (byte) 0xFF, (byte) 0x45);

    private TLVDecoder decoder;
    private DecodeSession session;

    @Before
    public void setUp() {
        decoder = new TLVDecoder();
        session = new DecodeSession();
        session.setTagMetaData(EmvTags.METADATA);
    }

    @Test
    public void readTag_hexTlv_tagRead() {
        String simpleHexTlv = TEST_SIMPLE_HEX_TLV_89;

        byte[] tlv = FormatUtils.fromHexString(simpleHexTlv);
        ByteBuffer bb = ByteBuffer.wrap(tlv);

        Tag tag = TlvParser.readTag(bb);
        Assert.assertEquals(TAG_89, tag);
    }

    @Test
    public void readTag_hexTlv_tagRead2() {
        String simpleHexTlv = TEST_SIMPLE_HEX_TLV_8A;

        byte[] tlv = FormatUtils.fromHexString(simpleHexTlv);
        ByteBuffer bb = ByteBuffer.wrap(tlv);

        Tag tag = TlvParser.readTag(bb);
        Assert.assertEquals(TAG_8A, tag);
    }

    @Test
    public void readTag_hexTlv_tagRead3() {
        String simpleHexTlv = TEST_SIMPLE_HEX_TLV_9F02;

        byte[] tlv = FormatUtils.fromHexString(simpleHexTlv);
        ByteBuffer bb = ByteBuffer.wrap(tlv);

        Tag tag = TlvParser.readTag(bb);
        Assert.assertEquals(TAG_9F02, tag);
    }

    @Test
    public void readTag_hexTlv_tagRead4() {
        String simpleHexTlv = TEST_SIMPLE_HEX_TLV_5F2A;

        byte[] tlv = FormatUtils.fromHexString(simpleHexTlv);
        ByteBuffer bb = ByteBuffer.wrap(tlv);

        Tag tag = TlvParser.readTag(bb);
        Assert.assertEquals(TAG_5F2A, tag);
    }

    @Test
    public void readTag_hexTlv_tagRead5() {
        String simpleHexTlv = TEST_SIMPLE_HEX_TLV_9F1C;

        byte[] tlv = FormatUtils.fromHexString(simpleHexTlv);
        ByteBuffer bb = ByteBuffer.wrap(tlv);

        Tag tag = TlvParser.readTag(bb);
        Assert.assertEquals(TAG_9F1C, tag);
    }

    @Test
    public void readTag_hexTlv_tagRead6() {
        String simpleHexTlv = TEST_SIMPLE_HEX_TLV_DFFF45;

        byte[] tlv = FormatUtils.fromHexString(simpleHexTlv);
        ByteBuffer bb = ByteBuffer.wrap(tlv);

        Tag tag = TlvParser.readTag(bb);
        Assert.assertEquals(TAG_DFFF45, tag);
    }

    @Test
    public void readLength_hexTlv_lengthRead() {
        String simpleHexTlv = TEST_SIMPLE_HEX_TLV_89;

        byte[] tlv = FormatUtils.fromHexString(simpleHexTlv);
        ByteBuffer bb = ByteBuffer.wrap(tlv);

        TlvParser.readTag(bb);
        Length length = TlvParser.readLength(bb);

        int expectedLength = TEST_SIMPLE_HEX_TLV_89_LENGTH;
        int actualLength = length.getLength();

        System.out.println("Length bytes (89): " +
                FormatUtils.toHexString(length.getBytes()));

        Assert.assertEquals(expectedLength, actualLength);
    }

    @Test
    public void readLength_hexTlv_lengthRead2() {
        String simpleHexTlv = TEST_SIMPLE_HEX_TLV_8A;

        byte[] tlv = FormatUtils.fromHexString(simpleHexTlv);
        ByteBuffer bb = ByteBuffer.wrap(tlv);

        TlvParser.readTag(bb);
        Length length = TlvParser.readLength(bb);

        int expectedLength = TEST_SIMPLE_HEX_TLV_8A_LENGTH;
        int actualLength = length.getLength();

        System.out.println("Length bytes (8A): " +
                FormatUtils.toHexString(length.getBytes()));

        Assert.assertEquals(expectedLength, actualLength);
    }

    @Test
    public void readLength_hexTlv_lengthRead3() {
        String simpleHexTlv = TEST_SIMPLE_HEX_TLV_9F02;

        byte[] tlv = FormatUtils.fromHexString(simpleHexTlv);
        ByteBuffer bb = ByteBuffer.wrap(tlv);

        TlvParser.readTag(bb);
        Length length = TlvParser.readLength(bb);

        int expectedLength = TEST_SIMPLE_HEX_TLV_9F02_LENGTH;
        int actualLength = length.getLength();

        System.out.println("Length bytes (9F02): " +
                FormatUtils.toHexString(length.getBytes()));

        Assert.assertEquals(expectedLength, actualLength);
    }

    @Test
    public void readLength_hexTlv_lengthRead4() {
        String simpleHexTlv = TEST_SIMPLE_HEX_TLV_5F2A;

        byte[] tlv = FormatUtils.fromHexString(simpleHexTlv);
        ByteBuffer bb = ByteBuffer.wrap(tlv);

        TlvParser.readTag(bb);
        Length length = TlvParser.readLength(bb);

        int expectedLength = TEST_SIMPLE_HEX_TLV_5F2A_LENGTH;
        int actualLength = length.getLength();

        System.out.println("Length bytes (5F2A): " +
                FormatUtils.toHexString(length.getBytes()));

        Assert.assertEquals(expectedLength, actualLength);
    }

    @Test
    public void readLength_hexTlv_lengthRead5() {
        String simpleHexTlv = TEST_SIMPLE_HEX_TLV_9F1C;

        byte[] tlv = FormatUtils.fromHexString(simpleHexTlv);
        ByteBuffer bb = ByteBuffer.wrap(tlv);

        TlvParser.readTag(bb);
        Length length = TlvParser.readLength(bb);

        int expectedLength = TEST_SIMPLE_HEX_TLV_9F1C_LENGTH;
        int actualLength = length.getLength();

        System.out.println("Length bytes (9F1C): " +
                FormatUtils.toHexString(length.getBytes()));

        Assert.assertEquals(expectedLength, actualLength);
    }

    @Test
    public void readLength_hexTlv_lengthRead6() {
        String simpleHexTlv = TEST_SIMPLE_HEX_TLV_DFFF45;

        byte[] tlv = FormatUtils.fromHexString(simpleHexTlv);
        ByteBuffer bb = ByteBuffer.wrap(tlv);

        TlvParser.readTag(bb);
        Length length = TlvParser.readLength(bb);

        int expectedLength = TEST_SIMPLE_HEX_TLV_DFFF45_LENGTH;
        int actualLength = length.getLength();

        System.out.println("Length bytes (DFFF45): " +
                FormatUtils.toHexString(length.getBytes()));

        Assert.assertEquals(expectedLength, actualLength);
    }

    @Test
    public void decode_simpleHexTlv_decoded() {
        String simpleHexTlv = TEST_SIMPLE_HEX_TLV_89;

        byte[] tlv = FormatUtils.fromHexString(simpleHexTlv);
        Map<Tag, Value> tagValues = TlvParser.decodeAsMap(tlv);

        Assert.assertTrue(tagValues.containsKey(TAG_89));
    }

    @Test
    public void decode_simpleHexTlv_decoded2() {
        String simpleHexTlv = TEST_SIMPLE_HEX_TLV_8A;

        byte[] tlv = FormatUtils.fromHexString(simpleHexTlv);
        Map<Tag, Value> tagValues = TlvParser.decodeAsMap(tlv);

        Assert.assertTrue(tagValues.containsKey(TAG_8A));
    }

    @Test
    public void decode_simpleHexTlv_decoded3() {
        String simpleHexTlv = TEST_SIMPLE_HEX_TLV_9F02;

        byte[] tlv = FormatUtils.fromHexString(simpleHexTlv);
        Map<Tag, Value> tagValues = TlvParser.decodeAsMap(tlv);

        Assert.assertTrue(tagValues.containsKey(TAG_9F02));
    }

    @Test
    public void decode_simpleHexTlv_decoded4() {
        String simpleHexTlv = TEST_SIMPLE_HEX_TLV_5F2A;

        byte[] tlv = FormatUtils.fromHexString(simpleHexTlv);
        Map<com.eightam.tlvparser.lib.Tag, Value> tagValues = TlvParser.decodeAsMap(tlv);

        Assert.assertTrue(tagValues.containsKey(TAG_5F2A));
    }

    @Test
    public void decode_simpleHexTlv_decoded5() {
        String simpleHexTlv = TEST_SIMPLE_HEX_TLV_9F1C;

        byte[] tlv = FormatUtils.fromHexString(simpleHexTlv);
        Map<Tag, Value> tagValues = TlvParser.decodeAsMap(tlv);

        Assert.assertTrue(tagValues.containsKey(TAG_9F1C));
    }

    @Test
    public void decode_simpleHexTlv_decoded6() {
        String simpleHexTlv = TEST_SIMPLE_HEX_TLV_DFFF45;

        byte[] tlv = FormatUtils.fromHexString(simpleHexTlv);
        Map<Tag, Value> tagValues = TlvParser.decodeAsMap(tlv);

        Assert.assertTrue(tagValues.containsKey(TAG_DFFF45));
    }

    @Test
    public void decode_complexHexTlv_decoded() {
        List<DecodedData> decodedDataList = decoder.decode(TEST_COMPLEX_HEX_TLV, 0, session);

        String authorisationCode = "";
        byte[] authorisationCodeByteArray = new byte[0];

        String authorisationResponseCode = "";
        byte[] authorisationResponseCodeByteArray = new byte[0];

        String transactionAmount = "";
        byte[] transactionAmountByteArray = new byte[0];

        String transactionCurrencyCode = "";
        byte[] transactionCurrencyCodeByteArray = new byte[0];

        String terminalId = "";
        byte[] terminalIdByteArray = new byte[0];

        String dfff45 = "";
        byte[] dfff45ByteArray = new byte[0];

        for (DecodedData decodedData : decodedDataList) {
            BerTlv berTlv = decodedData.getTlv();

            if (berTlv != null) {
                io.github.binaryfoo.tlv.Tag tag = berTlv.getTag();

                if (io.github.binaryfoo.tlv.Tag.fromHex("89").equals(tag)) {
                    authorisationCode = decodedData.getDecodedData();
                    authorisationCodeByteArray = berTlv.getValue();

                } else if (EmvTags.AUTHORISATION_RESPONSE_CODE.equals(tag)) {
                    authorisationResponseCode = decodedData.getDecodedData();
                    authorisationResponseCodeByteArray = berTlv.getValue();

                } else if (EmvTags.AMOUNT_AUTHORIZED.equals(tag)) {
                    transactionAmount = decodedData.getDecodedData();
                    transactionAmountByteArray = berTlv.getValue();

                } else if (EmvTags.TRANSACTION_CURRENCY_CODE.equals(tag)) {
                    transactionCurrencyCode = decodedData.getDecodedData();
                    transactionCurrencyCodeByteArray = berTlv.getValue();

                } else if (EmvTags.TERMINAL_ID.equals(tag)) {
                    terminalId = decodedData.getDecodedData();
                    terminalIdByteArray = berTlv.getValue();

                } else if (io.github.binaryfoo.tlv.Tag.fromHex("DFFF45").equals(tag)) {
                    dfff45 = decodedData.getDecodedData();
                    dfff45ByteArray = berTlv.getValue();
                }
            }
        }

        byte[] tlv = FormatUtils.fromHexString(TEST_COMPLEX_HEX_TLV);
        Map<Tag, Value> tagValues = TlvParser.decodeAsMap(tlv);

        System.out.println("Authorisation code: " + authorisationCode);
        System.out.println("Authorisation response code: " + authorisationResponseCode);
        System.out.println("Transaction amount: " + transactionAmount);
        System.out.println("Transaction currency code: " + transactionCurrencyCode);
        System.out.println("Terminal ID: " + terminalId);
        System.out.println("DFFF45: " + dfff45);

        Assert.assertTrue(tagValues.containsKey(TAG_89));
        Assert.assertTrue(tagValues.containsKey(TAG_8A));
        Assert.assertTrue(tagValues.containsKey(TAG_9F02));
        Assert.assertTrue(tagValues.containsKey(TAG_5F2A));
        Assert.assertTrue(tagValues.containsKey(TAG_9F1C));
        Assert.assertTrue(tagValues.containsKey(TAG_DFFF45));

        Assert.assertArrayEquals(authorisationCodeByteArray, tagValues.get(TAG_89).getValue());
        Assert.assertArrayEquals(authorisationResponseCodeByteArray, tagValues.get(TAG_8A).getValue());
        Assert.assertArrayEquals(transactionAmountByteArray, tagValues.get(TAG_9F02).getValue());
        Assert.assertArrayEquals(transactionCurrencyCodeByteArray, tagValues.get(TAG_5F2A).getValue());
        Assert.assertArrayEquals(terminalIdByteArray, tagValues.get(TAG_9F1C).getValue());
        Assert.assertArrayEquals(dfff45ByteArray, tagValues.get(TAG_DFFF45).getValue());
    }

}
